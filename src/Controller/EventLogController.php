<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Entity\EventLog;
use App\Repository\EventLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\User;

use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;


class EventLogController extends AbstractController
{
    #[Route('/event-log', name: 'event_log_index', methods: ['GET'])]
    public function index(Request $request, EventLogRepository $eventLogRepository,  SerializerInterface $serializer): Response
    {
        $currentPage = $request->query->getInt('page', 1);
        $maxPerPage = 10;

        $queryBuilder = $eventLogRepository->createQueryBuilder('e');
        $adapter = new QueryAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta->setCurrentPage($currentPage);
        $pagerfanta->setMaxPerPage($maxPerPage);

        $events = [];
        foreach ($pagerfanta->getCurrentPageResults() as $event) {
            $events[] = $event;
        }

        $data = [
            'events' => $events,
            'current_page' => $pagerfanta->getCurrentPage(),
            'total_items' => $pagerfanta->getNbResults(),
            'total_pages' => $pagerfanta->getNbPages(),
        ];

        // dd($events);
        $json = $serializer->serialize($data, 'json', [AbstractNormalizer::GROUPS => ['event_log']]);
        return new Response($json, 200, ['Content-Type' => 'application/json']); 
       }
    

    #[Route('/event-log/new', name: 'event_log_new', methods: ['POST'])]
    public function new(Request $request, EntityManagerInterface $em, UserInterface $user, ValidatorInterface $validator, SerializerInterface $serializer): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $data = json_decode($request->getContent(), true);
        $eventLog = new EventLog();
        $eventLog->setTitle($data['title']);
        $eventLog->setDescription($data['description']);
        $eventLog->setTimestamp(new \DateTime());
        $eventLog->setUser($user);
        $errors = $validator->validate($eventLog);
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }
        $em->persist($eventLog);
        $em->flush();

        $json = $serializer->serialize($eventLog, 'json', [AbstractNormalizer::GROUPS => ['event_log']]);
        return $this->json($json, 201);
    }

    #[Route('/event-log/{id}/edit', name: 'event_log_edit', methods: ['PUT'])]
    public function edit(Request $request, EventLog $eventLog, EntityManagerInterface $em, ValidatorInterface $validator, SerializerInterface $serializer): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $data = json_decode($request->getContent(), true);
        $eventLog->setTitle($data['title']);
        $eventLog->setDescription($data['description']);
        $errors = $validator->validate($eventLog);
        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        $em->flush();
        $json = $serializer->serialize($eventLog, 'json', [AbstractNormalizer::GROUPS => ['event_log']]);
        return $this->json($json, 201);
    }

    #[Route('/event-log/{id}', name: 'event_log_delete', methods: ['DELETE'])]
    public function delete(EventLog $eventLog, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $em->remove($eventLog);
        $em->flush();
        return $this->json(null, 204);
    }


    #[Route('/test-relation', name: 'test_relation', methods: ['Post'])]
    public function addEventlogToUser(Request $request,  EntityManagerInterface $em, SerializerInterface $serializer) : Response
    {
        $eventLog = new EventLog();
        $data = json_decode($request->getContent(), true);
        $eventLog->setTitle($data['title']);
        $eventLog->setDescription($data['description']);
        $user_id = $data['user_id'];
        $eventLog->setTimestamp(new \DateTime());
        $user = $em->getRepository(User::class)->find($user_id);
        $user->addEventLog($eventLog);
        $em->persist($user);
        $em->flush();
        $json = $serializer->serialize($user, 'json', [AbstractNormalizer::GROUPS => ['event_log']]);
        return new Response($json, 200, ['Content-Type' => 'application/json']); 

    }
    
    #[Route('/event-log/search', name: 'event_log_search', methods: ['GET'])]
    public function search(Request $request, EventLogRepository $eventLogRepository, SerializerInterface $serializer): Response
    {
        $search = $request->query->get('search', null);
        $filters = $request->query->all();
        unset($filters['search']); 

        $queryBuilder = $eventLogRepository->createSearchQueryBuilder($search, $filters);
        $events = $queryBuilder->getQuery()->getResult();

        $data = [
            'events' => $events,
        ];

        $json = $serializer->serialize($data, 'json', [AbstractNormalizer::GROUPS => ['event_log']]);
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }
}



    