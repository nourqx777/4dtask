<?php

namespace App\Repository;

use App\Entity\EventLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

// use Pagerfanta\Doctrine\ORM\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Doctrine\ORM\QueryBuilder;



/**
 * @extends ServiceEntityRepository<EventLog>
 */
class EventLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventLog::class);
    }


    public function findPaginated(int $page = 1, int $maxPerPage = 10): Pagerfanta
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $adapter = new QueryAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta->setCurrentPage($page);
        $pagerfanta->setMaxPerPage($maxPerPage);

        return $pagerfanta;
    }



    public function createSearchQueryBuilder(?string $search = null, ?array $filters = []): QueryBuilder
    {
        $qb = $this->createQueryBuilder('e');

        if ($search) {
            $qb->andWhere('e.title LIKE :search')
               ->setParameter('search', '%' . $search . '%');
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                $qb->andWhere(sprintf('e.%s = :%s', $key, $key))
                   ->setParameter($key, $value);
            }
        }

        return $qb;
    }

//    /**
//     * @return EventLog[] Returns an array of EventLog objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EventLog
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
